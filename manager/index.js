const UserRepository = require('../infrastructure/user/UserRepository');

const validUserToken = async (payload) => {
  const userValid = await new UserRepository().getByIdUser(payload);
  return userValid;
}

module.exports = { validUserToken };
