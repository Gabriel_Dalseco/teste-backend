const express = require('express');

const UserRepository = require('../../infrastructure/user/UserRepository');
const createJWT = require('../../service/createJWT');
const { validRegisterMiddleware } = require('../../middlewares/registerValid');

const router = express.Router();

const register = async (req, res) => {
  try {
    const { password, ...data } = await new UserRepository().registerUser(req.body);
    
    const token = createJWT(data);
    return res.status(201).json({ message: 'Usuário criado com sucesso', token });
  } catch (err) {
    if (err.name === 'SequelizeUniqueConstraintError')
      return res.status(400).json({ message: 'Email ou nome de usuário indisponível' });

    return res.status(500).json({ message: err.name, error: err.message });
  }
};

router.post('/register', validRegisterMiddleware, register);

module.exports = { registerRouter: router };
