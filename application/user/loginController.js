const express = require('express');

const UserRepository = require('../../infrastructure/user/UserRepository');

const createJWT = require('../../service/createJWT');
const { validLoginMiddleware } = require('../../middlewares/loginValid');

const router = express.Router();

const login = async (req, res) => {
  try {
    const user = await new UserRepository().login(req.body);
  
    const token = createJWT(user);
    res.status(200).json({ token });
  } catch (err) {
    if (err.message === 'EmailOrPassordInvalid')
      return res.status(400).json({ message: 'Usuário ou senha inválido' });

    return res.status(500).json({ message: err.name, error: err.message });
  }
};

router.post('/login', validLoginMiddleware, login);

module.exports = { loginRouter: router };
