const express = require('express');

const UserRepository = require('../../infrastructure/user/UserRepository');
const createJWT = require('../../service/createJWT');
const { userValidMiddleware, newDataUserMiddleware } = require('../../middlewares/userValid');

const router = express.Router();

const updateUser = async (req, res) => {
  try {
    const updateUser = await new UserRepository().updateUserData(req.body, req.payload);

    const token = createJWT(updateUser);
    return res.status(200).json({ message: 'Dados de usuário atualizados', token });
  } catch (err) {
    if (err.message === 'UserUpdateIsNotValid')
      return res.status(400).json({ message: 'Email ou nome de usuário indisponível' });

    return res.status(500).json({ message: err.name, error: err.message });
  }
};

const deactivateUser = async (req, res) => {
  try {
    await new UserRepository().disableUser(req.payload);

    return res.status(200).json({ message: 'Usuário desativado' });
  } catch (err) {
    if (err.message === 'exclusionIsNotValid')
      return res.status(400).json({ message: 'Usuário não cadastrado' });

    return res.status(500).json({ message: err.name, error: err.message });
  }
};

router.put('/:id', userValidMiddleware, newDataUserMiddleware, updateUser);
router.patch('/:id', userValidMiddleware, newDataUserMiddleware, deactivateUser);

module.exports = { userRouter: router };
