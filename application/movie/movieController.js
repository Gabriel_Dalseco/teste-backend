const express = require('express');

const MovieRepository = require('../../infrastructure/movie/MovieRepository');
const { userValidMiddleware } = require('../../middlewares/userValid');

const router = express.Router();

const getAllMovies = async (req, res) => {
  try {
    const getMovies = await new MovieRepository().getAllMovies(req.payload);

    return res.status(200).json(getMovies);
  } catch (err) {
    if (err.message === 'userIsNotValid')
      return res.status(400).json({ message: 'Usuário não cadastrado' });

    return res.status(500).json({ message: err.name, error: err.message });
  }
};

const getByIdMovie = async (req, res) => {
  try {
    const getDetailMovie = await new MovieRepository().getByIdMovie(req.params.id, req.payload);

    return res.status(200).json(getDetailMovie);
  } catch (err) {
    if (err.message === 'movieNotFound')
      return res.status(400).json({ message: 'Filme não encontrado' });
    
    if (err.message === 'userIsNotValid')
      return res.status(400).json({ message: 'Usuário não cadastrado' });

    return res.status(500).json({ message: err.name, error: err.message });
  }
};

router.get('/', userValidMiddleware, getAllMovies);
router.get('/:id', userValidMiddleware, getByIdMovie);

module.exports = { movieRouter: router };
