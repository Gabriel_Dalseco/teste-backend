const { loginRouter } = require('../application/user/loginController');
const { registerRouter } = require('../application/user/registerController');
const { userRouter } = require('../application/user/userController');
const { movieRouter } = require('../application/movie/movieController');

module.exports = {
    loginRouter,
    registerRouter,
    userRouter,
    movieRouter,
};
