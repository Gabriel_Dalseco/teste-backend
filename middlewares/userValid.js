const { errorReadingJWT } = require('./rescues');
const tokenValid = require('../service/decryptJWT');
const utils = require('../service/utils');

const userValidMiddleware = errorReadingJWT((req, res, next) => {
  const token = req.headers.authorization;

  if (!token) return res.status(401).json({ message: 'Usuário nao autenticado' });

  const validToken = token.substring(7);
  const payload = tokenValid(validToken);

  req.payload = payload;

  return next();
});

const updateUserMiddleware = (req, res, next) => {
  const { username, email, password } = req.body;
  if (
    !utils.isEmailValid(email)
    || !utils.isPasswordValid(password)
    || !utils.isNameValid(username)
  ) return res.status(400).json({ message: 'Campos inválidos' });

  next();
};

const newDataUserMiddleware = (req, res, next) => {
  const token = req.headers.authorization;
  const userId = req.params.id;

  const validToken = token.substring(7);
  const payload = tokenValid(validToken);

  if (Number(userId) !== payload.id) return res.status(401).json({ message: 'Usuário nao autenticado' }); 

  return next();
};

module.exports = {
  userValidMiddleware,
  updateUserMiddleware,
  newDataUserMiddleware,
};
