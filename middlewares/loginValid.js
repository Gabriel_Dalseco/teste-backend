const { isEmailValid, isPasswordValid } = require('../service/utils');

const validLoginMiddleware = (req, res, next) => {
  const { email, password } = req.body;

  if (!isEmailValid(email) || !isPasswordValid(password))
    return res.status(400).json({ message: 'Campos inválidos' });

  return next();
}

module.exports = { validLoginMiddleware };
