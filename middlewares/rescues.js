exports.errorReadingJWT = fn => async (req, res, next) => {
  try {
    await fn(req, res, next);
  } catch (err) {
    res.status(422).json({ message: err.name, error: err.message });
  }
};
