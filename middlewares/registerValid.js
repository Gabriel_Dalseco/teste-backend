const utils = require('../service/utils');

const validRegisterMiddleware = (req, res, next) => {
  const { username, email, password, role } = req.body;
  if (
    !utils.isRoleValid(role)
    || !utils.isEmailValid(email)
    || !utils.isPasswordValid(password)
    || !utils.isNameValid(username)
  ) return res.status(400).json({ message: 'Campos inválidos' });

  next();
}

module.exports = { validRegisterMiddleware };
