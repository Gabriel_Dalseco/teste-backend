const Movie = (sequelize, DataTypes) => {
  const Movie = sequelize.define('Movie', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    movieTitle: DataTypes.STRING,
    originalTitle: DataTypes.STRING,
    length: DataTypes.INTEGER,
    releaseYearBrazil: DataTypes.DATE,
    rating: DataTypes.DOUBLE,
    trailer: DataTypes.STRING,
    image: DataTypes.STRING,
  });

  Movie.associate = (models) => {
    Movie.belongsTo(models.Director, { foreignKey: 'director_id' });
  };

  return Movie;
};

module.exports = Movie;
