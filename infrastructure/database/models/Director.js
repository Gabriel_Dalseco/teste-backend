const Director = (sequelize, DataTypes) => {
  const Director = sequelize.define('Director', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
  });

  Director.associate = (models) => {
    Director.hasMany(models.Movie, { foreignKey: 'id' });
  };

  return Director;
};

module.exports = Director;
