'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('Movies', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      movieTitle: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      originalTitle: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      length: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      releaseYearBrazil: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      rating: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      trailer: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'arquivo de video',
      },
      image: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'imagem.png',
      },
      director_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Directors',
          key: 'id',
        },
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Movies');
  },
};
