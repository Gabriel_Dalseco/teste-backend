'use strict';

module.exports = {
  up: async queryInterface => (
    queryInterface.bulkInsert(
      'Users',
      [
        {
          username: 'tryber',
          password: 'U2FsdGVkX18xIfpGVQzKPU8tL0YiZcoKEKNit0nuxAc=',
          email: 'tryber@gmail.com',
          role: 'admin',
        },
        {
          username: 'teste',
          password: 'U2FsdGVkX18xIfpGVQzKPU8tL0YiZcoKEKNit0nuxAc=',
          email: 'teste@gmail.com',
          role: 'client',
        },
        {
          username: 'joao',
          password: 'U2FsdGVkX18xIfpGVQzKPU8tL0YiZcoKEKNit0nuxAc=',
          email: 'joao@gmail.com',
          role: 'client',
        },
      ],
      {},
    )
  ),

  down: async queryInterface => queryInterface.bulkDelete('Users', null, {}),
};
