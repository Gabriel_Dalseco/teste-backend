'use strict';

module.exports = {
  up: async queryInterface => (
    queryInterface.bulkInsert(
      'Directors',
      [
        {
          name: 'Francis Ford Coppola',
        },
        {
          name: 'Christopher Nolan',
        },
        {
          name: 'Todd Phillips',
        },
      ],
      {},
    )
  ),

  down: async queryInterface => queryInterface.bulkDelete('Directors', null, {}),
};

