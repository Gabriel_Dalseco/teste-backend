'use strict';

module.exports = {
  up: async queryInterface => (
    queryInterface.bulkInsert(
      'Movies',
      [
        {
          movieTitle: 'O Poderoso Chefão',
          originalTitle: 'The Godfather',
          length: '175',
          releaseYearBrazil: '1972/09/10',
          rating: '9.2',
          trailer: 'arquivo de video',
          image: 'o-poderoso-chefao.png',
          director_id: 1,
        },
        {
          movieTitle: 'A origem',
          originalTitle: 'Inception',
          length: '148',
          releaseYearBrazil: '2010/08/06',
          rating: '8.8',
          trailer: 'arquivo de video',
          image: 'a-origem.png',
          director_id: 2,
        },
        {
          movieTitle: 'Coringa',
          originalTitle: 'Joker',
          length: '122',
          releaseYearBrazil: '2019/10/03',
          rating: '8.5',
          trailer: 'arquivo de video',
          image: 'o-coringa.png',
          director_id: 3,
        }
      ],
      {},
    )
  ),

  down: async queryInterface => queryInterface.bulkDelete('Movies', null, {}),
};
