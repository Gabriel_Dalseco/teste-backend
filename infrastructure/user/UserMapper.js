
const UserMapper = {
  toEntity({ dataValues }) {
    const { id, username, email, role, status } = dataValues;
    return { id, username, email, role, status };
  },

  toDatabase(survivor) {
    const { id, username, password, email, role, status } = survivor;
    return { id, username, password, email, role, status };
  },
};

module.exports = UserMapper;
