const UserMapper = require('./UserMapper');
const { User } = require('../database/models');

const { encrypt, decrypt } = require('../../service/crypto');

class UserRepository {
  async _registerUser(username, encryptPassword, email) {
    const updateStatus = await User.update(
      UserMapper.toDatabase({ username, password: encryptPassword, status: true }),
      { where: { email: email } },
    );

    if (updateStatus[0] === 0) return false;

    const { dataValues: { password, ...data } } = await User.findOne({ where: { email } });
    return data;
  }

  async registerUser(users) {
    const { username, password, email, role } = users;
    const encryptPassword = encrypt(password);

    const updateUser = await this._registerUser(username, encryptPassword, email);

    if (updateUser) return updateUser;

    const { dataValues } = await User.create(UserMapper.toDatabase({
      username, password: encryptPassword, email, role,
    }));

    const { dataValues: newUser } = await User.findOne({ where: { email: dataValues.email } });

    return newUser;
  }

  async _updateUserData(dataUser, payload) {
    const { username, email, password } = dataUser;
    const encryptPassword = encrypt(password);

    const updateStatus = await User.update(
      UserMapper.toDatabase({ username, email, password: encryptPassword }),
      { where: { id: payload.id, email: payload.email, status: true } },
    );

    if (updateStatus[0] === 0) throw new Error('UserUpdateIsNotValid');

    const { dataValues } = await User.findOne({ where: { id: payload.id, email } });
    return dataValues;
  }

  async updateUserData(dataUser, payload) {
    const { password, ...newDataUser } = await this._updateUserData(dataUser, payload);
    return newDataUser;
  }

  async _disableUser(payload) {
    const updateStatus = await User.update(
      UserMapper.toDatabase({ status: false }),
      { where: { id: payload.id, email: payload.email } },
    );

    if (updateStatus[0] === 0) throw new Error('exclusionIsNotValid');
    return;
  }

  async disableUser(payload) {
    await this._disableUser(payload);
    return;
  }

  async _loginValidEmail(email, password) {
    const findEmail = await User.findOne({ where: { email } });
    if (!findEmail || !findEmail.status) throw new Error('EmailOrPassordInvalid');

    const unencryptedPassword = decrypt(findEmail.password);

    if (unencryptedPassword !== password) throw new Error('EmailOrPassordInvalid');

    return findEmail;
  }

  async login({ email, password }) {
    const user = await this._loginValidEmail(email, password);
    return UserMapper.toEntity(user);
  }

  async _getByIdUser({id, email}) {
    const findEmail = await User.findOne({ where: { id, email, status: true } });
    if (!findEmail) throw new Error('userIsNotValid');

    return findEmail;
  }

  async getByIdUser(payload) {
    const user = await this._getByIdUser(payload);
    return UserMapper.toEntity(user);
  }
}

module.exports = UserRepository;
