const MovieMapper = require('./MovieMapper');
const { Movie } = require('../database/models');
const { Director } = require('../database/models');

const { validUserToken } = require('../../manager/index');

class MovieRepository {
  async getAllMovies(payload) {
    const userValid = await validUserToken(payload);
    if (!userValid) return userValid;

    const movies = await Movie.findAll({
      include: [Director],
    });

    return movies.map(MovieMapper.toEntity);
  }

  async _getByIdMovie(id) {
    const user = await Movie.findByPk(id, { include: [Director] });
    if (!user) throw new Error('movieNotFound');
    return user;
  }

  async getByIdMovie(id, payload) {
    const userValid = await validUserToken(payload);
    if (!userValid) return userValid;

    const user = await this._getByIdMovie(id);

    return MovieMapper.toEntity(user);
  }
}

module.exports = MovieRepository;
