const MovieMapper = {
  toEntity({ dataValues }) {
    const {
      id,
      movieTitle,
      originalTitle,
      length,
      releaseYearBrazil,
      rating,
      trailer,
      image,
      Director: director,
    } = dataValues;

    return {
      id,
      movieTitle,
      originalTitle,
      length,
      releaseYearBrazil,
      rating,
      trailer,
      image,
      director,
    };
  },

  toDatabase(survivor) {
    const {
      movieTitle,
      originalTitle,
      length,
      releaseYearBrazil,
      rating,
    } = survivor;

    return { movieTitle, originalTitle, length, releaseYearBrazil, rating };
  },
};

module.exports = MovieMapper;
