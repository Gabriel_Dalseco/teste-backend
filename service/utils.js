function isEmailValid(email = '') {
  const regex = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email.toLowerCase());
}

function isPasswordValid(password = '') {
  return password.length >= 6 &&  password.length <= 12;
}

function isRoleValid(role = '') {
  const roleOptions = ['admin', 'client'];
  return roleOptions.includes(role);
}

function isNameValid(name = '') {
  const regex = /^[a-zA-Z-\s]{3,40}$/;
  return regex.test(name);
}

module.exports = {
  isEmailValid,
  isPasswordValid,
  isRoleValid,
  isNameValid,
}
