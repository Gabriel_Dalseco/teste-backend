# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos.

# 🏗 O que fazer?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

# 🚨 Requisitos

- A API deverá ser construída em **NodeJS** ou **Rails**
- Implementar autenticação e deverá seguir o padrão **JWT**, lembrando que o token a ser recebido deverá ser no formato **Bearer**
- Caso seja desenvolvida em NodeJS o seu projeto terá que ser implementado em **ExpressJS** ou **SailsJS**
- Para a comunicação com o banco de dados utilize algum **ORM**/**ODM**
- Bancos relacionais permitidos:
  - MySQL
  - MariaDB
  - Postgre
- Bancos não relacionais permitidos:
  - MongoDB
- Sua API deverá seguir os padrões Rest na construção das rotas e retornos
- Sua API deverá conter a collection/variáveis do postman ou algum endpoint da documentação em openapi para a realização do teste

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto
- Segurança da API, como autenticação, senhas salvas no banco, SQL Injection e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção [O que desenvolver?](##--o-que-desenvolver)
- Migrations para a criação das tabelas do banco relacional

# 🎁 Extra

Esses itens não são obrigatórios, porém desejados.

- Testes unitários
- Linter
- Code Formater

**Obs.: Lembrando que o uso de algum linter ou code formater irá depender da linguagem que sua API for criada**

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deve conter as seguintes features:

- Admin

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Usuário

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Filmes

  - Cadastro (Somente um usuário administrador poderá realizar esse cadastro)
  - Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
  - Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores)
  - Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.: Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é admin ou não**

# 🔗 Links

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:

  1. https://expressjs.com/pt-br/
  2. https://sailsjs.com/

- Guideline rails http://guides.rubyonrails.org/index.html

# 🗝️ Utilização dotenv (Variávies de ambiente)

- O arquivo envExample, exemplifica a utilização das variávies

# 📃 Comandos Sequelize utilizados

### Criação de Migrations

  - npx sequelize migration:generate --name create-users
  - npx sequelize migration:generate --name create-directos
  - npx sequelize migration:generate --name create-movies

### Criação de Seeders

  - npx sequelize seed:generate --name users
  - npx sequelize seed:generate --name directors
  - npx sequelize seed:generate --name movies

### Adicionando ao banco de dados

  - npx sequelize db:migrate (criar tabelas)
  - npx sequelize db:migrate:undo:all (reverter ação de criação)

  - npx sequelize db:seed (popular banco)
  - npx sequelize db:seed:undo:all (reverter ação de popular banco)

# 🗃️ Documentação API (Rotas)

## POST localhost:3001/user/register

### Request

- Um email será considerado válido se tiver o formato `<prefixo>@<domínio>`.

- A senha deverá conter pelo menos 6 caracteres.

- O corpo da requisição deverá seguir o formato abaixo:

```json
body:
{
  "username": "nome-de-Usuário",
  "email": "email@mail.com",
  "password": "123456",
  "role": "client" || "admin"
}
```

### Response

- Success

```json
{
  "message": "Usuário cadastrado com sucesso",
  "token": "JSON WEBTOKEN"
}
```

- Fail

```json
{
  "message": "dados para cadastro invalidos"
}
```

## POST localhost:3001/user/login

### Request

- O corpo da requisição deverá seguir o formato abaixo:

```json
body:
{
  "email": "email@mail.com",
  "password": "135982"
}
```

### Response

- Success

```json
{
  "token": "JSON WEBTOKEN"
}
```

- Fail

```json
{
  "message": "Usuário ou senha inválido"
}
```

## PUT localhost:3001/user/:id

### Request

```json
body:
{
  "username": "nome-de-Usuário",
  "email": "email@mail.com",
  "password": "123456"
}
```

### Response

- Success

```json
{
  "message": "Dados de usuário atualizados",
  "token": "JSON WEBTOKEN"
}
```

- Fail

```json
{
  "message": "Email ou nome de usuário indisponível"
}
```

```json
{
  "message": "Campos inválidos"
}
```

## PATCH localhost:3001/user/:id

- Success

```json
{
  "message": "Usuário desativado"
}
```

- Fail

```json
{
  "message": "Usuário não cadastrado"
}
```

## GET localhost:3001/movie

### Response

- Success

```json
[
  {
    "id": 1,
    "movieTitle": "O Poderoso Chefão",
    "originalTitle": "The Godfather",
    "length": 175,
    "releaseYearBrazil": "1972-09-10T00:00:00.000Z",
    "rating": 9.2,
    "trailer": "arquivo de video",
    "image": "o-poderoso-chefao.png",
    "director": {
      "id": 1,
      "name": "Francis Ford Coppola"
    }
  },
  {
    "id": 2,
    "movieTitle": "A origem",
    "originalTitle": "Inception",
    "length": 148,
    "releaseYearBrazil": "2010-08-06T00:00:00.000Z",
    "rating": 8.8,
    "trailer": "arquivo de video",
    "image": "a-origem.png",
    "director": {
      "id": 2,
      "name": "Christopher Nolan"
    }
  },
  {
    "id": 3,
    "movieTitle": "Coringa",
    "originalTitle": "Joker",
    "length": 122,
    "releaseYearBrazil": "2019-10-03T00:00:00.000Z",
    "rating": 8.5,
    "trailer": "arquivo de video",
    "image": "o-coringa.png",
    "director": {
      "id": 3,
      "name": "Todd Phillips"
    }
  }
]
```

- Fail

```json
{
  "message": "Usuário nao autenticado"
}
```

## GET localhost:3001/movie/:id

### Response

- Success

```json
{
"id": 1,
  "movieTitle": "O Poderoso Chefão",
  "originalTitle": "The Godfather",
  "length": 175,
  "releaseYearBrazil": "1972-09-10T00:00:00.000Z",
  "rating": 9.2,
  "trailer": "arquivo de video",
  "image": "o-poderoso-chefao.png",
  "director": {
      "id": 1,
      "name": "Francis Ford Coppola"
}
```

- Fail

```json
{
  "message": "Usuário nao autenticado"
}
```

```json
{
  "message": "filme nao encontrado"
}
```

## POST localhost:3001/movie (Não implementado)

### Request

- O campos não poderão ser nulos

- Somente o usuário admin estará autorizado para cadastrar filmes

```json
body:
{
  "movieTitle": "O Poderoso Chefão",
  "originalTitle": "The Godfather",
  "length": "175",
  "releaseYearBrazil": "1972/09/10",
  "rating": "9.2",
  "trailer": "arquivo de video",
  "image": "o-poderoso-chefao.png",
  "director_id": "1"
}
```

### Response

- Success

```json
{
  "message": "filme cadastrado com sucesso"
}
```

- Fail

```json
{
  "message": "Usuário nao autenticado"
}
```
